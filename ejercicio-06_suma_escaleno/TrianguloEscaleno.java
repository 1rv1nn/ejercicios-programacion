/**
 * Clase que implementa el perimétro de un triangulo escaleno
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

import java.util.Scanner;
/*Ejercicio 6 */
class TrianguloEscaleno{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Triangulo Escaleno \nIngresa la longitud de la base: ");
        int a=sc.nextInt();
        System.out.println("Ingresa la longitud del lado lateral: ");
        int b=sc.nextInt();
        System.out.println("Ingresa la longitud del lado opuesto: ");
        int c=sc.nextInt();
        int perimetro=a+b+c;
        System.out.println("El perimetro del Triangulo Escaleno es de: "+perimetro);
        sc.close();
    }
}