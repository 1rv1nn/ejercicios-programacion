/**
 * Ejercicios básicos de programación 
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

import java.util.Scanner;

/* Ejercicio 2 */

class SumaConsecutivos{

    /**
     * Método consecutivos
     * @param num 
     * @return la suma de números
     */
    public static void consecutivos (int num){
        int aux=0;
        for(int i=0;i<=num;i++){
           aux=aux+i;
           
        }
        System.out.println("La suma de los números consecutivos de "+num + " es = "+aux);
    }    

    public static void main(String[] args) {
        SumaConsecutivos.consecutivos(3);
    }
} 