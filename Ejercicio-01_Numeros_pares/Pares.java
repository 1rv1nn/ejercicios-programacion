/**
 * Ejercicios básicos de programación 
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

class Pares{

    public static void main(String[] args) {
        for(int i=0;i<=100;i++){
            if(i%2==0)
            System.out.println(i);
        } 
    }
   

}