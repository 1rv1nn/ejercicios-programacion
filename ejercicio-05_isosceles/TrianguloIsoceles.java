/**
 * Clase que calcula el perimétro de un triangulo isoceles
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

import java.util.Scanner;
/*Ejercicio 5 */
class TrianguloIsoceles{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Triangulo Isoceles \nIngresa la longitud de lados iguales: ");
        int valor1=sc.nextInt();
        int ladoIgual=2*valor1;
        System.out.println("Ingresa la longitud del lado diferente: ");
        int valor2=sc.nextInt();
        int ladoDif=valor2;
        int perimetro=ladoIgual+ladoDif; // reenombre de variable por cuestion de estetica.
        System.out.println("El perimetro del Triangulo Isoceles es de: "+perimetro);
        sc.close();
    }
}