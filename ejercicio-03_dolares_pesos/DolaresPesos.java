/**
 * Ejercicios básicos de programación 
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

import java.util.Scanner;

/*Ejercicio 3 */
class DolaresPesos{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("Ingresa el monto en dolares: $");
        int dollar=sc.nextInt();
        double convertion=dollar*20.12;
        System.out.println("$"+dollar + " es equivalente a "+convertion+" pesos mexicanos");
        sc.close();
    }
}