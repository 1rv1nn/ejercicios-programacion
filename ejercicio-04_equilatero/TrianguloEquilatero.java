/**
 * Clase que calcula el perimétro de un triangulo equilatero
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */

import java.util.Scanner;
/*Ejercicio 4 */
class TrianguloEquilatero{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Triangulo equilatero \nIngresa la longitud de los lados: ");
        int valor=sc.nextInt();
        int perimetro=3*valor;
        System.out.println("El perimetro del Triangulo equilatero es de: "+perimetro);
        sc.close();
    }
}