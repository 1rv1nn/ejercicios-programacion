Observaciones importantes :

            0.El código ejecutable se encuentra el \src.

            1.Dado que los pseudocódigos de los algoritmos para encontrar los perimétros de los respectivos triangulos ya se encuentran en el repositorio,
              este pseudocódigo se basará principalmente en el clase main del ejercicio 8. 

            2.El programa cumple con uno de los Principios SOLID, Open-Close(Establece que las entidades software (clases, módulos y funciones) 
              deberían estar abiertos para su extensión, pero cerrados para su modificación.) por lo cual esté fue una de las principales 
              motivaciones al implementar dicho programa.

            3.El ejercicio cuenta con 1 diagrama de clases y 1 diagrama de actividades de la clase main.     
    