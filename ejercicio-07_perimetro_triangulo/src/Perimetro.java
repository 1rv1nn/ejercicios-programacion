/**
 * Clase que calcula el perimétro de un triangulo Isoceles
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023-1
 * @version 1.0
 */


class Perimetro extends Triangulo{


    @Override
    int perimetroEquilatero(int valor){
        int perimetro=3*valor;
        return perimetro;
    }

    @Override
    int perimetroIsoceles(int valor1,int valor2){
        int ladoIgual=2*valor1;
        int ladoDif=valor2;
        int perimetro = ladoIgual+ladoDif;
        return perimetro;
        
        
    }

    @Override
    int perimetroEscaleno(int a,int b,int c){
        int perimetro=a+b+c;
        return perimetro;
    }

    
}