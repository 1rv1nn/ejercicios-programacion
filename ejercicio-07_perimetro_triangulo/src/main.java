import java.util.Scanner;
import static java.lang.System.*;

public class main {
    static void menu(){
        out.println("\t |Perimétros| \nElige el triangulo que deseas saber su perimétro \n 1.Triangulo Equilatero \n 2.Triangulo Isoceles \n 3.Triangulo Escaleno \n 4.Salir");
    }
    public static void main(String[] args) {
    Perimetro equilatero= new Perimetro();
    Perimetro isoceles= new Perimetro();
    Perimetro escaleno= new Perimetro();
    //System.out.println(escaleno.perimetroEscaleno(3, 3, 1));
    Scanner sc=new Scanner(in);
    int opcion; 
    do{
        menu();
        opcion=sc.nextInt();
        switch(opcion){
            case 1:
                out.println("Ingresa el valor de los lados del triangulo Equilatero");
                int value=sc.nextInt();
                out.println("El perimétro es de " +equilatero.perimetroEquilatero(value));
                break;
            case 2: 
                out.println("Ingresa el valor de los lados iguales");
                int value1=sc.nextInt();
                out.println("Ingresa el valor del lado diferente");
                int value2=sc.nextInt();
                out.println("El perimétro es de " +isoceles.perimetroIsoceles(value1,value2));
                break;  
            case 3:
                out.println("Ingresa el valor de la base");
                int value3=sc.nextInt();
                out.println("Ingresa el valor del lado lateral");
                int value4=sc.nextInt();
                out.println("Ingresa el valor del lado opuesto");
                int value5=sc.nextInt();
                out.println("El perimétro es de " +escaleno.perimetroEscaleno(value3,value4,value5));
                break;       
                
            case 4: // salir
                out.println("Hasta luego.");
                break;    

            default:
                out.println("Hasta luego");   
        } 
        
    }while(opcion!=4);
    }    
}
  

