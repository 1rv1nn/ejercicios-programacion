/**
 * Clase abstracta que
 * 
 * @author Cruz González Irvin Javier
 * @since IS 2023
 * @version 1.0
 */

 public abstract class Triangulo {
    

    /**
     * método para calcular el area de un triangulo escaleno
     * @param valor lados
     * @return perimetro
     */
    abstract int perimetroEquilatero(int valor);

    /**
     * método para calcular el area de un triangulo isoceles
     * @param valor1 lados iguales
     * @param valor2 lado diferente
     * @return perimetro
     */
    abstract int perimetroIsoceles(int valor1,int valor2);


     /**
     * método para calcular el area de un triangulo escaleno
     * @param a base
     * @param b lado lateral
     * @param c lado opuesto
     * @return perimetro
     */
    abstract int perimetroEscaleno(int a,int b,int c);
    

 }